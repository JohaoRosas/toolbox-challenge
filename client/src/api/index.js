 

class API {
  static request(api, method, body) {
    const data = {
      method,
      headers: {
        'Content-Type': 'application/json'
      },
    } 
    if (method === 'POST' || method === 'PUT') {
      data.body = JSON.stringify(body)
    }

    return new Promise((resolve, reject) => {
      fetch(api, data)
        .then((response) => { 
          if (response.status >= 400) {
            throw new Error('error')
          }
          return response.json()
        })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

export default API

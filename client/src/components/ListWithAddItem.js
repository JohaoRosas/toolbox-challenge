import React, { useState } from 'react';
import { Col, Form, Row, Button, Card } from 'react-bootstrap'
import API from '../api';
import { useForm } from 'react-hook-form'
const initialList = [];

  const listColors = ['Primary',
  'Secondary',
  'Success',
  'Danger',
  'Warning',
  'Info', 
  'Dark',];

export const ListWithAddItem = () => {
  const [value, setValue] = useState('');
  const [list, setList] = useState(initialList); 

  const handleChange = event => {
    setValue(event.target.value);
  };
  
  const URL_API = 'http://localhost:2000/iecho?text=';

  const getWordReverseAndPalindrome = async () => {
    try { 
      const response = await API.request(URL_API + value, 'GET');
        
      if (response) { 
        if (value) { 
         setList(list.concat(response));
         setValue('');
        }

      } else { 
        return null;
      }
    } catch (_) { 
    }
  }

  const {
    handleSubmit, // Callback que envía el evento
  } = useForm()


  return (
    <>
      <Form onSubmit={handleSubmit(getWordReverseAndPalindrome)}>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="formText">
            <Form.Control value={value} onChange={handleChange} type="text" placeholder="Insert Text" />
          </Form.Group>
          <Button type="submit" className="mb-2"  >
            Send
      </Button>
        </Row>
      </Form>

      <Row xs={1} md={2} className="g-4">
        {list.slice(0).reverse().map((item,index) => (
          <Col>
            <Card
              bg={listColors[Math.floor(Math.random() *6) + 1].toLowerCase()}
              key={index}
              text={item.text.toLowerCase() === 'light' ? 'dark' : 'white'}
              style={{ width: '18rem' }}
              className="mb-2"
            >
              <Card.Header>{item.text}</Card.Header>
              <Card.Body>
                <Card.Title>{item.isPalindrome?"Es palindromo":"No es palindromo"} </Card.Title>
                <Card.Text>
              </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>


    </>
  );
};

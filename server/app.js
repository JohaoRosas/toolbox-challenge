var express = require('express');
var app = express(); 

const Error = require('./utils/error.js');
const operationtService = require('./services/operation');  

 

// Configurar cabeceras y cors
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
	if ('OPTIONS' === req.method) {
		res.send(200);
	} else {
		next();
	}
});

app.get('/', function (req, res) {
	res.send('Bienvenido a toolbox TV');
});
 
var bodyParser = require('body-parser') //npm install body-parser

app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json({
	limit: '40mb'
}));

 
app.use(bodyParser.json());
 
var server = app.listen(2000, () => {
	console.log('server is running on port', server.address().port);
});

app.use( "", operationtService); 

function close() {
    app.close()
}


// Handler error
app.use(errorHandler);

function errorHandler(err, req, res, next) {
	let errorResult = null;
	if (err instanceof Error) {
       
		errorResult = { 
			error: err.message
		};
	} else {
		// General Error 
		errorResult = { 
			error :   err.message
		};
	}

	res.status(err.status || 500).json(errorResult);
}


module.exports = app;
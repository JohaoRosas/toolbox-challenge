const expect = require('chai').expect;

const OperationController = require('../controller/operation');
const operationController = new OperationController();


const app = require('../app');

const request = require('supertest');


describe('function get palindrome and reverse', function () {

    it('GET reverse & palindrome OK', async () => {

        const text = "RECONOCER";

        const response = await operationController.process(text);
        expect(response).to.be.an('Object');
        expect(response.text).to.be.an('String');

        expect(response.text).to.equal(text);
        expect(response.isPalindrome).to.equal(true);
    });

    it('GET reverse &  palindrome ERROR', async () => {

        const text = "HOLA";

        const response = await operationController.process(text);
        expect(response).to.be.an('Object');
        expect(response.text).to.be.an('String');

        expect(response.text).to.not.equal(text);
        expect(response.isPalindrome).to.not.equal(true);
    });


    describe('GET /iecho?text=unanu', function () {

        it('respond 200', function (done) {
            request(app)
                .get('/iecho?text=unanu')
                .expect('Content-Type', /json/)
                .expect(200,done) 
        });

 
    });

    describe('GET /iecho?text', function () {
        it('responds 400', function (done) {
            request(app)
                .get('/iecho?text=')
                .expect('Content-Type', /json/)
                .expect(400,done)  
        });
    });


});

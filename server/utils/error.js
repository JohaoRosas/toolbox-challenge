class ErrorToolbox extends Error {
	/**
	 * Constructor Custom Error 
	 * @param {*} status [400 - 401 - and others] 
	 * @param {*} message [System Message] 
	 */
	constructor(status, message, cause) { 
		super(message);

		//Error.captureStackTrace(this, this.constructor);

		this.name = this.constructor.name;
		this.status = status || 500;

		this.message = message ||
			"Ocurrió un error inesperado. Por favor, intenta de nuevo";
 

		if (cause) {
			this.cause = cause;
		}
	}
}




module.exports = ErrorToolbox;


   class Word {

    constructor() {
		 
	}

    isPalindrome(letters) {

        var characters  = letters.toLowerCase().split(''),
            firstLetter = characters.shift(),
            lastLetter  = characters.pop();
    
        if (firstLetter !== lastLetter) {
            return false;
        }
    
        if (characters.length < 2) {
            return true;
        }
    
        return this.isPalindrome(characters.join(''));
    
    }

    reverse(letters) {

        return letters.split('').reverse().join('');
    
    }

   }

   module.exports = Word;
const ErrorToolbox = require('../utils/error');
const Word = require('../utils/word');
const word = new Word();  


class OperationController {

    constructor() {
		 
	}

    async process(text) { 
        try
        {
            if(!text)
            throw new ErrorToolbox(400, "No Text");
 
            const wordReverse = word.reverse(text);

            const isPalindrome =  word.isPalindrome(text)
            

            return {
                text : wordReverse,
                isPalindrome : isPalindrome
            }


        } catch (ex) {
			if (ex instanceof Error) {
				throw ex;
			} else {
				throw new Error(400, 0, 10, ex.message, "Error in process controller", ex);
			}
		} finally {
		}

    }

}

module.exports = OperationController;

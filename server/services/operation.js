const express = require('express');
const router = express.Router();

const OperationController = require('../controller/operation');
const operationController = new OperationController(); 

const ErrorToolbox = require('../utils/error');

router.get('/iecho', async function (req, res, next) {
 
  try {   
    if (!req.query.text) {
        throw new ErrorToolbox(400, "No Text");
    }

    const result = await operationController.process(req.query.text);

    res.status(200).json(result);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
